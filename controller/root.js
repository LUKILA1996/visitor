const db = require('../model/db')

const getRoot = (req, res) => {
  res.status(200).render('pages/index')
}

module.exports = { getRoot }
