const db = require('../model/db')

const createLeave = async (req, res) => {
  const { id_card, name, address, nationality } = req.body

  const enter = await db('guest_leave').insert({
    id_card,
    name,
    address,
    nationality,
  })

  return res.redirect('/leave')
}

const getLeave = async (req, res) => {
  const leave = await db('guest_leave').select(
    'guest_leave_id',
    'id_card',
    'name',
    'address',
    'nationality',
    'arrival_date',
    'arrival_time',
  )
  return res.status(200).render('pages/leave', { data: leave })
}

const getLeaveDetail = async (req, res) => {
  const { id } = req.params
  const leave = await db('guest_leave')
    .select(
      'guest_leave_id',
      'id_card',
      'name as name',
      'address as address',
      'nationality as nationality',
      'arrival_date',
      'arrival_time',
    )
    .where({ guest_leave_id: id })

  return res.status(200).render('pages/ledet', { data: leave })
}

const getLeaveEdit = async (req, res) => {
  const { id } = req.params

  const leave = await db('guest_leave')
    .select(
      'guest_leave_id',
      'id_card',
      'name as name',
      'address as address',
      'nationality as nationality',
      'arrival_date',
      'arrival_time',
    )
    .where({ guest_leave_id: id })

  return res.status(200).render('pages/ledit', { data: leave })
}

const updateLeave = async (req, res) => {
  const { id } = req.params

  const { id_card, name, address, nationality } = req.body

  const leave = await db('guest_leave')
    .where({
      guest_leave_id: id,
    })
    .update({
      id_card,
      name,
      address,
      nationality,
    })

  return res.status(200).redirect('/leave')
}

const deleteLeave = async (req, res) => {
  const { id } = req.params

  const leave = await db('guest_leave')
    .where({
      guest_leave_id: id,
    })
    .del()

  return res.status(200).redirect('/leave')
}

module.exports = {
  createLeave,
  getLeave,
  getLeaveDetail,
  updateLeave,
  deleteLeave,
  getLeaveEdit,
}
