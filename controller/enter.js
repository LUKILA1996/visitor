const db = require('../model/db')

const createEnter = async (req, res) => {
  const {
    id_card,
    name,
    address,
    nationality,
  } = req.body

  const enter = await db('guests_enter').insert({
    id_card,
    name,
    address,
    nationality,
  })

  return res.redirect('/enter')
}

const getEnter = async (req, res) => {
  const enter = await db('guests_enter').select(
    'guests_enter_id',
    'id_card',
    'name',
    'address',
    'nationality',
    'arrival_date',
    'arrival_time',
  )
  return res.status(200).render('pages/enter', { data: enter })
}

const getEnterDetail = async (req, res) => {
  const { id } = req.params
  const enter = await db('guests_enter')
    .select(
      'guests_enter_id',
      'id_card',
      'name as name',
      'address as address',
      'nationality as nationality',
      'arrival_date',
      'arrival_time',
    )
    .where({ guests_enter_id: id })

  return res.status(200).render('pages/endet', { data: enter })
}

const getEnterEdit = async (req, res) => {
  const { id } = req.params

  const enter = await db('guests_enter')
  .select(
    'guests_enter_id',
    'id_card',
    'name as name',
    'address as address',
    'nationality as nationality',
    'arrival_date',
    'arrival_time',
  )
  .where({ guests_enter_id: id })

  return res.status(200).render('pages/endit', { data: enter })
}

const updateEnter = async (req, res) => {
  const { id } = req.params

  const {
    id_card,
    name,
    address,
    nationality,
  } = req.body

  const enter = await db('guests_enter')
    .where({
      guests_enter_id: id,
    })
    .update({
      id_card,
      name,
      address,
      nationality,
    })

  return res.status(200).redirect('/enter')
}

const deleteEnter = async (req, res) => {
  const { id } = req.params

  const enter = await db('guests_enter')
    .where({
      guests_enter_id: id,
    })
    .del()

  return res.status(200).redirect('/enter')
}

module.exports = {
  createEnter,
  getEnter,
  getEnterDetail,
  updateEnter,
  deleteEnter,
  getEnterEdit,
}
