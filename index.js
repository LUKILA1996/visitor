const express = require('express')
const app = express()
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const router = require('./routes')

app.use(router)

app.set('view engine', 'ejs')
app.use(express.static('public'))

app.listen(3000, () => {
  console.log(`Visitor application running`)
})
