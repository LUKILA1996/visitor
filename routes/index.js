const router = require('express').Router()
const enterRouter = require('./enter')
const leaveRouter = require('./leave')
const rootRouter = require('./root')

router.use('/', rootRouter)
router.use('/enter', enterRouter)
router.use('/leave', leaveRouter)

module.exports = router
