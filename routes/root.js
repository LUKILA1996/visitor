const router = require('express').Router()
const root = require('../controller/root')

router.get('/', root.getRoot)

module.exports = router