const router = require('express').Router()
const leave = require('../controller/leave')

router.post('/', leave.createLeave)
router.get('/', leave.getLeave)
router.get('/:id', leave.getLeaveDetail)
router.get('/edit/:id', leave.getLeaveEdit)
router.post('/edit/:id', leave.updateLeave)
router.get('/delete/:id', leave.deleteLeave)

module.exports = router
