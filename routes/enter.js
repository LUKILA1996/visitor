const router = require('express').Router()
const enter = require('../controller/enter')

router.post('/', enter.createEnter)
router.get('/', enter.getEnter)
router.get('/:id', enter.getEnterDetail)
router.get('/edit/:id', enter.getEnterEdit)
router.post('/edit/:id', enter.updateEnter)
router.get('/delete/:id', enter.deleteEnter)

module.exports = router
