/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('address').del()
  await knex('address').insert([
    {address: 'a', neighbourhood: 'bhg', hamlet: '123', urban_village: '135', sub_district: '1658', city_or_regency: '9465', province: 'sadhj'},
    {address: 'a', neighbourhood: 'bhg', hamlet: '123', urban_village: '135', sub_district: '1658', city_or_regency: '9465', province: 'sadhj'},
    {address: 'a', neighbourhood: 'bhg', hamlet: '123', urban_village: '135', sub_district: '1658', city_or_regency: '9465', province: 'sadhj'},
  ]);
};
