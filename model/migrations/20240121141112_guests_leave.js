/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable('guests_leave', (table) => {
    table.increments('guests_leave_id').primary()
    table.string('id_card', 16).notNullable()
    table.date('departure_date').defaultTo(knex.fn.now())
    table.time('departure_time').defaultTo(knex.fn.now())
  })
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {}
