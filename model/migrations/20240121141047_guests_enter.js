/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable('guests_enter', (table) => {
    table.increments('guests_enter_id').primary()
    table.string('id_card', 16).notNullable()
    table.string('name', 50).notNullable()
    table.string('address', 75).notNullable()
    table.string('nationality', 25).notNullable()
    table.date('arrival_date').defaultTo(knex.fn.now())
    table.time('arrival_time').defaultTo(knex.fn.now())
  })
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {}
