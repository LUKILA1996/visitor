/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable('address', (table) => {
    table.increments('address_id').primary()
    table.string('address', 75).notNullable()
    table.string('neighbourhood', 25)
    table.string('hamlet', 25)
    table.string('urban_village', 25)
    table.string('sub_district', 25)
    table.string('city_or_regency', 25)
    table.string('province', 25)
  })
}

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {}
